using UnityEngine;

namespace Gloxinia.Viker.Bike
{
    /// <summary>
    /// バイクにモノを載せるクラス
    /// </summary>
    internal class Career : MonoBehaviour
    {
        [SerializeField]
        private Bike m_bike;

        /// <summary>
        /// モノの親要素となる Transform
        /// </summary>
        [SerializeField]
        private Transform m_cereerTransform;

        private void Awake()
        {
            Heroをバイクに乗せる();
        }

        private void Heroをバイクに乗せる()
        {
            // Hero の参照が取れない場合 何もしない
            if (m_bike.Hero == null) return;

            // 今のところ問答無用で Hero をバイクに載せる
            // TODO: 乗り降りできるようにしたとき移植する必要がある
            m_bike.Hero.transform.parent = m_cereerTransform;
        }
    }
}
