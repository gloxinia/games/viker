#nullable enable

using Gloxinia.Viker.XRIT;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

namespace Gloxinia.Viker.Bike
{
    /// <summary>
    /// バイクのグリップ部分のクラス
    ///
    /// 各部名称
    /// スロットル: 手の平で握ってまわす部分 アクセルに使用する
    /// レバー: 指の先で握る部分 ブレーキとクラッチに使用する
    /// </summary>
    internal class Grip : XRBaseInteractable
    {
        [Header("BikeGrip ならではのインスペクタ")]

        [SerializeField]
        private GameObject m_グリップ用の手のモデル = null!;

        [SerializeField]
        private GameObject m_スロットルのモデル = null!;

        /// <summary>
        /// 触れていないときは null になる
        /// </summary>
        private GVDirectInteractor? m_触れた手の参照;

        /// <summary>
        /// 触れていないときは Vector3.zero になる
        /// </summary>
        private Vector3 m_触れはじめたとき手の前方向 = Vector3.zero;

        /// <summary>
        /// 握っていないときは Vector3.zero になる
        /// </summary>
        private Vector3 m_スロットルを握りはじめたときの手の前方向 = Vector3.zero;

        /// <summary>
        /// 手をひねるとはグリップに触れながらその場で回転させること
        /// 触れていないときは 0f になる
        /// </summary>
        internal float 手をひねった角度 { get; private set; } = 0f;

        /// <summary>
        /// 手前にひねることができる限界の角度
        /// 手前にひねると負の値になることに注意する
        /// </summary>
        [SerializeField]
        private float m_スロットルの最小角度 = -60f;

        /// <summary>
        /// 奥にひねることができる限界の角度
        /// </summary>
        [SerializeField]
        private float m_スロットルの最大角度 = 0f;

        /// <summary>
        /// スロットルをひねるとはバイクのアクセルの動作のこと
        /// 具体的にはグリップを Grip ボタンで握りながらその場で回転させること
        /// 握っていないときは 0f になる
        /// </summary>
        internal float スロットルをひねった角度 { get; private set; } = 0f;

        /// <summary>
        /// スロットルの強さがこれより強い場合のみスロットルをひねっている扱いにする
        /// "スロットルをひねった角度" や "スロットルの強さ" には関係しない
        /// あくまで "スロットルをひねっているか" 用の閾値
        /// </summary>
        [SerializeField]
        private float スロットルの遊び = 0.1f;

        /// <summary>
        /// スロットルをひねった角度を 0f ~ 1f に正規化した値
        /// </summary>
        internal float スロットルの強さ => スロットルの強さを取得する();

        internal bool スロットルをひねっているか => スロットルの強さ > スロットルの遊び;

        /// <summary>
        /// これより強い場合のみレバーを握っている扱いにする
        /// "レバーの強さ" には関係しない
        /// あくまで "レバーを握っているか" 用の閾値
        /// </summary>
        [SerializeField]
        private float レバーの遊び = 0.1f;

        internal float レバーの強さ { get; private set; } = 0f;

        internal bool レバーを握っているか => レバーの強さ > レバーの遊び;

        [SerializeField]
        private InputActionProperty m_レバーを握る;

        #region Message

        protected override void Awake()
        {
            base.Awake();

            m_レバーを握る.action.Enable();
            グリップ用の手のモデルを非表示にする();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            m_レバーを握る.action.Disable();
        }

        private void FixedUpdate()
        {
            // 触れている場合
            if (isHovered)
            {
                手をひねった角度 = 手を何度ひねっているかを取得する(m_触れた手の参照!.transform.forward);
                グリップ用の手のモデルを回転させる();

                レバーの強さ = レバーの強さを取得する();
                //レバーのモデルを回転させる();

                // 触れている かつ 握っている場合
                if (isSelected)
                {
                    スロットルをひねった角度 = スロットルを何度ひねっているかを取得する(m_触れた手の参照!.transform.forward);
                    //スロットルのモデルを回転させる();
                }
            }
        }

        #endregion

        #region Override

        protected override void OnHoverEntered(HoverEnterEventArgs args)
        {
            base.OnHoverEntered(args);

            // HeroHand が触れた場合
            if (args.interactorObject is GVDirectInteractor gvdi)
            {
                m_触れた手の参照 = gvdi;
                gvdi.HeroHand.手のモデルを非表示にする();
                グリップ用の手のモデルを表示する();

                // グリップに触れはじめたときの手の前方向を保持する
                m_触れはじめたとき手の前方向 = args.interactorObject.transform.forward;
            }
        }

        protected override void OnHoverExited(HoverExitEventArgs args)
        {
            base.OnHoverExited(args);

            // HeroHand が離れた場合
            if (args.interactorObject is GVDirectInteractor gvdi)
            {
                m_触れた手の参照 = null;
                gvdi.HeroHand.手のモデルを表示する();
                グリップ用の手のモデルを非表示にする();

                // リセット処理
                m_触れはじめたとき手の前方向 = Vector3.zero;
                スロットルをひねった角度 = 0f;
                //スロットルのモデルを回転させる();

            }
        }

        protected override void OnSelectEntered(SelectEnterEventArgs args)
        {
            base.OnSelectEntered(args);

            // スロットルを握りはじめたときの手の前方向を保持する
            m_スロットルを握りはじめたときの手の前方向 = args.interactorObject.transform.forward;
        }

        protected override void OnSelectExited(SelectExitEventArgs args)
        {
            base.OnSelectExited(args);

            // リセット処理
            m_スロットルを握りはじめたときの手の前方向 = Vector3.zero;
            スロットルをひねった角度 = 0f;
            //スロットルのモデルを回転させる();
        }

        #endregion

        private void グリップ用の手のモデルを表示する()
        {
            m_グリップ用の手のモデル.SetActive(true);
        }

        private void グリップ用の手のモデルを非表示にする()
        {
            m_グリップ用の手のモデル.SetActive(false);
        }

        private float 手を何度ひねっているかを取得する(Vector3 触れている手の前方向)
        {
            var 平面の法線ベクトル = transform.right;
            var 平面上の触れはじめの手の前方向 = Vector3.ProjectOnPlane(m_触れはじめたとき手の前方向, 平面の法線ベクトル);
            var 平面上の触れている手の前方向 = Vector3.ProjectOnPlane(触れている手の前方向, 平面の法線ベクトル);
            return Vector3.SignedAngle(平面上の触れはじめの手の前方向, 平面上の触れている手の前方向, 平面の法線ベクトル);
        }

        private float スロットルを何度ひねっているかを取得する(Vector3 握っている手の前方向)
        {
            var 平面の法線ベクトル = transform.right;
            var 平面上の握りはじめの手の前方向 = Vector3.ProjectOnPlane(m_スロットルを握りはじめたときの手の前方向, 平面の法線ベクトル);
            var 平面上の握っている手の前方向 = Vector3.ProjectOnPlane(握っている手の前方向, 平面の法線ベクトル);
            var ひねった角度 = Vector3.SignedAngle(平面上の握りはじめの手の前方向, 平面上の握っている手の前方向, 平面の法線ベクトル);

            // 角度を 最小 ~ 最大 の間で丸めて返す
            return Mathf.Clamp(ひねった角度, m_スロットルの最小角度, m_スロットルの最大角度);
        }

        private float スロットルの強さを取得する()
        {
            // 50度ひねったら最大になる感じにしたいので 1/50 = 0.02f にしておく
            var いい感じの倍率 = 0.02f;

            // 倍率をかけて 0f ~ 1f の間で丸めて返す
            var ひねった角度の絶対値 = Mathf.Abs(スロットルをひねった角度);
            return Mathf.Clamp01(ひねった角度の絶対値 * いい感じの倍率);
        }

        private float レバーの強さを取得する()
        {
            return m_レバーを握る.action.ReadValue<float>();
        }

        private void グリップ用の手のモデルを回転させる()
        {
            var rotation = Quaternion.Euler(手をひねった角度, 0f, 0f);
            m_グリップ用の手のモデル.transform.localRotation = rotation;
        }

        private void スロットルのモデルを回転させる()
        {
            var rotation = Quaternion.Euler(スロットルをひねった角度, 0f, 0f);
            m_スロットルのモデル.transform.localRotation = rotation;
        }

        private void レバーのモデルを回転させる()
        {
            //
        }
    }
}
