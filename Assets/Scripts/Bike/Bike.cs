#nullable enable

using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Gloxinia.Viker.Bike
{
    using Hero = Hero.Hero;

    /// <summary>
    /// バイクの各機能へのアクセスポイントとしての役割を持つクラス
    /// </summary>
    public class Bike : MonoBehaviour
    {
        [SerializeField]
        private Hero? m_hero;
        internal Hero? Hero => m_hero;

        #region Self References

        [SerializeField]
        private Rigidbody m_rigidbody = null!;
        internal Rigidbody Rigidbody => m_rigidbody;

        [SerializeField]
        private Move m_move = null!;
        internal Move Move => m_move;

        [SerializeField]
        private Turn m_turn = null!;
        internal Turn Turn => m_turn;

        [SerializeField]
        private Grip m_gripLeft = null!;
        internal Grip GripLeft => m_gripLeft;

        [SerializeField]
        private Grip m_gripRight = null!;
        internal Grip GripRight => m_gripRight;

        [SerializeField]
        private WheelCollider m_wheelFront = null!;
        internal WheelCollider WheelFront => m_wheelFront;

        [SerializeField]
        private WheelCollider m_wheelBack = null!;
        internal WheelCollider WheelBack => m_wheelBack;

        [SerializeField]
        private Transform m_期待される頭の位置 = null!;

        #endregion

        internal bool 待機状態である { get; private set; } = false;
        internal bool 走行状態である { get; private set; } = false;

        #region Message

        private void Awake()
        {
            // 重心を下げる
            Rigidbody.centerOfMass = new Vector3(0f, -0.5f, 0f);

            m_カメラをリセットする.action.Enable();
            m_カメラをリセットする.action.performed += 頭の位置をバイクのハンドル前に移動する;
        }

        private void OnDestroy()
        {
            m_カメラをリセットする.action.performed -= 頭の位置をバイクのハンドル前に移動する;
            m_カメラをリセットする.action.Disable();
        }

        private void FixedUpdate()
        {
            時速の情報を更新する();

            // FIXME: ステートマシーンの導入
            if (キロメートル毎時 < 5f)
            {
                if (待機状態である == false)
                {
                    Debug.Log("Bike: 待機状態に移行します");
                    待機状態である = true;
                    走行状態である = false;
                }
            }
            else
            {
                if (走行状態である == false)
                {
                    Debug.Log("Bike: 走行状態に移行します");
                    走行状態である = true;
                    待機状態である = false;
                }
            }
        }

        #endregion

        #region 時速関連

        internal float キロメートル毎時 { get; private set; } = 0f;

        [SerializeField]
        private float m_何秒ごとに時速を更新するか = 0.25f;

        private float m_前に時速を更新してから何秒経ったか = 0f;

        private void 時速の情報を更新する()
        {
            m_前に時速を更新してから何秒経ったか += Time.fixedDeltaTime;

            if (m_何秒ごとに時速を更新するか < m_前に時速を更新してから何秒経ったか)
            {
                var メートル毎秒 = Rigidbody.velocity.magnitude;
                キロメートル毎時 = メートル毎秒 * 3.6f; // 3.6 = 60 * 60 / 1000
                m_前に時速を更新してから何秒経ったか = 0f;
            }
        }

        #endregion

        #region カメラリセット関連

        [SerializeField]
        private InputActionProperty m_カメラをリセットする;

        private void 頭の位置をバイクのハンドル前に移動する(InputAction.CallbackContext callbackContext)
        {
            // Hero の参照が取れない場合 何もしない
            if (Hero == null) return;

            // 実際の頭の位置から期待される頭の位置へのベクトルを計算する
            var 頭の位置 = Hero.XROrigin.Camera.transform.position;
            var 期待される頭の位置 = m_期待される頭の位置.position;
            var 頭の位置から期待される頭の位置へのベクトル = 期待される頭の位置 - 頭の位置;

            // 頭の位置がバイクの原点上にくるようにする
            // Hero Prefab 内の Camera Adjuster の位置を変更することで実現する
            Hero.CameraAdjuster.transform.Translate(頭の位置から期待される頭の位置へのベクトル);
        }

        #endregion
    }
}
