#nullable enable

using UnityEngine;

namespace Gloxinia.Viker.Bike
{
    /// <summary>
    /// バイクの移動機能を司るクラス
    /// </summary>
    internal class Move : MonoBehaviour
    {
        [SerializeField]
        private Bike m_bike = null!;

        #region message

        private void FixedUpdate()
        {
            if (m_bike.GripRight.スロットルをひねっているか)
            {
                アクセルをかける();
            }
            else
            {
                アクセルを切る();
            }

            if (m_bike.GripRight.レバーを握っているか)
            {
                ハンドブレーキをかける();
            }
            else if (m_bike.キロメートル毎時 > 5f)
            {
                エンジンブレーキをかける();
            }
            else
            {
                ブレーキを切る();
            }
        }

        #endregion

        private void アクセルをかける()
        {
            // アクセルをある程度ふかしている場合 後輪にアクセルをかける
            // TODO: ギアごとの最大トルクを取得する
            var アクセルの最大トルク = 500f;
            var アクセルの強さ = m_bike.GripRight.スロットルの強さ;
            var アクセルのトルク = アクセルの最大トルク * アクセルの強さ;
            m_bike.WheelBack.motorTorque = アクセルのトルク;
        }

        private void アクセルを切る()
        {
            m_bike.WheelBack.motorTorque = 0f;
        }

        private void ハンドブレーキをかける()
        {
            // 後輪にブレーキをかける
            var ハンドブレーキの最大トルク = 50000f;
            var ハンドブレーキの強さ = m_bike.GripRight.レバーの強さ;
            var ブレーキのトルク = ハンドブレーキの最大トルク * ハンドブレーキの強さ;
            m_bike.WheelBack.brakeTorque = ブレーキのトルク;
        }

        private void エンジンブレーキをかける()
        {
            // 後輪にブレーキをかける
            // TODO: ギアごとのエンジンブレーキのトルクを取得する
            var エンジンブレーキのトルク = 100f;
            m_bike.WheelBack.brakeTorque = エンジンブレーキのトルク;
        }

        private void ブレーキを切る()
        {
            m_bike.WheelBack.brakeTorque = 0f;
        }
    }
}
