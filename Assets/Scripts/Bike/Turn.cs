#nullable enable

using UnityEngine;

namespace Gloxinia.Viker.Bike
{
    /// <summary>
    /// バイクの旋回機能を司るクラス
    /// </summary>
    internal class Turn : MonoBehaviour
    {
        [SerializeField]
        private Bike m_bike = null!;

        [SerializeField]
        private GameObject m_ハンドルのルート = null!;

        /// <summary>
        /// 体の傾きの絶対値がこれより強い場合のみ体が傾いている扱いにする
        /// 例えば遊びが 0.05f の場合 -0.05f ~ 0.05f の体の傾きは傾いている扱いにならない
        ///  あくまで "体が傾いているか" 用の閾値
        /// </summary>
        [SerializeField]
        private float 体の傾きの遊び = 0.05f;

        /// <summary>
        /// 頭の位置の左右のずれを -1f ~ 1f に正規化した値
        /// </summary>
        internal float 体の傾きの強さ => 体の傾きの強さを取得する();

        internal bool 体が傾いているか => Mathf.Abs(体の傾きの強さ) > 体の傾きの遊び;

        internal float ステアリングの効き具合 => ステアリングの効き具合を取得する();

        private void FixedUpdate()
        {
            if (体が傾いているか)
            {
                ステアリングを操作する();
            }
            else
            {
                ステアリングをニュートラルに戻す();
            }

            ハンドルのモデルを傾ける();
        }

        private void ステアリングを操作する()
        {
            var ステアリングの最大角度 = 30f;
            var ステアリングの角度 = ステアリングの最大角度 * 体の傾きの強さ * ステアリングの効き具合;
            m_bike.WheelFront.steerAngle = ステアリングの角度;
        }

        private void ステアリングをニュートラルに戻す()
        {
            m_bike.WheelFront.steerAngle = 0f;
        }

        private void ハンドルのモデルを傾ける()
        {
            var 体を最大まで傾けたときにハンドルが曲がる角度 = 15f;
            var ハンドルを曲げる角度 = 体の傾きの強さ * 体を最大まで傾けたときにハンドルが曲がる角度;
            var rotation = Quaternion.Euler(0f, ハンドルを曲げる角度, 0f);
            m_ハンドルのルート.transform.localRotation = rotation;
        }

        /// <summary>
        /// 体の傾きの強さ を -1f ~ 1f に正規化した値を返す
        /// 傾きとあるが Main Carame の位置によって計算している
        /// Main Camera の傾きは利用していないことに注意する
        ///
        /// FIXME: カーブしたあとに傾きが減る現象を直す
        /// </summary>
        private float 体の傾きの強さを取得する()
        {
            // Hero の参照が取れない場合 0f を返す
            if (m_bike.Hero == null) return 0f;

            // バイクの上方向を法線とする平面上でどれだけ動いているかを取得する
            var 基準点 = transform.position;
            var 頭の位置 = m_bike.Hero.XROrigin.Camera.transform.position;
            var 平面の法線ベクトル = transform.up;
            var 頭の位置基準点からどれだけ離れているか = Vector3.ProjectOnPlane(頭の位置 - 基準点, 平面の法線ベクトル);

            // 0.5m 動いたら最大になる感じにしたいので 1/0.5 = 2f にしておく
            var いい感じの倍率 = 2f;

            // X 座標に倍率をかけて -1f ~ 1f の間で丸めて返す
            return Mathf.Clamp(頭の位置基準点からどれだけ離れているか.x * いい感じの倍率, -1f, 1f);
        }

        /// <summary>
        /// 速度が出るほどステアリングが効かなくなる
        /// </summary>
        private float ステアリングの効き具合を取得する()
        {
            var ステアリングが効かなくなる速度 = 100f; // km/h
            var ステアリングの効き具合の最低値 = 0.05f;

            if (ステアリングが効かなくなる速度 < m_bike.キロメートル毎時)
            {
                // 効かなくなる速度を超えている場合
                // 最低値を返す
                return ステアリングの効き具合の最低値;
            }
            else
            {
                // 効かなくなる速度を超えていない場合
                // 速度が 0f に近づくほど 1f に近づく値を返す
                return 1f - m_bike.キロメートル毎時 / ステアリングが効かなくなる速度;
            }
        }
    }
}
