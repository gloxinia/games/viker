#nullable enable

using Gloxinia.Viker.XRIT;
using System.Collections;
using UnityEngine;

namespace Gloxinia.Viker.Bike
{
    /// <summary>
    /// バイクの振動機能を司るクラス
    /// </summary>
    internal class Haptics : MonoBehaviour
    {
        [SerializeField]
        private Bike m_bike = null!;

        [SerializeField]
        private float 速度を振動の強さに変換する倍率 = 0.01f;

        private void Awake()
        {
            StartCoroutine(待機状態の振動());
            StartCoroutine(走行状態の振動());
        }

        private void OnDestroy()
        {
            StopCoroutine(待機状態の振動());
            StopCoroutine(走行状態の振動());
        }

        private IEnumerator 待機状態の振動()
        {
            while (true)
            {
                if (m_bike.待機状態である)
                {
                    グリップに触れている手を振動させる(0.3f, 0.05f);
                    yield return new WaitForSeconds(0.3f);

                    グリップに触れている手を振動させる(0.1f, 0.05f);
                    yield return new WaitForSeconds(0.2f);
                }
                else
                {
                    yield return null;
                }
            }
        }

        private IEnumerator 走行状態の振動()
        {
            while (true)
            {
                if (m_bike.走行状態である)
                {
                    // TODO: 振動の間隔も速度に連動させる
                    var 振動の強さ = Mathf.Clamp(m_bike.キロメートル毎時 * 速度を振動の強さに変換する倍率, 0f, 1f);

                    yield return new WaitForSeconds(0.1f);
                    グリップに触れている手を振動させる(振動の強さ, 0.02f);

                    yield return new WaitForSeconds(0.08f);
                    グリップに触れている手を振動させる(振動の強さ * 0.5f, 0.02f);
                }
                else
                {
                    yield return null;
                }
            }
        }

        private void グリップに触れている手を振動させる(float amplitude, float duration)
        {
            // Hero の参照が取れない場合 何もしない
            if (m_bike.Hero == null) return;

            グリップに触れている特定の手を振動させる(m_bike.Hero.LeftHand, amplitude, duration);
            グリップに触れている特定の手を振動させる(m_bike.Hero.RightHand, amplitude, duration);
        }

        private void グリップに触れている特定の手を振動させる(GVDirectInteractor hand, float amplitude, float duration)
        {
            if (hand.IsHovering(m_bike.GripLeft) || hand.IsHovering(m_bike.GripRight))
            {
                hand.SendHapticImpulse(amplitude, duration);
            }
        }
    }
}
