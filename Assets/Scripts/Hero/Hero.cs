#nullable enable

using Gloxinia.Viker.XRIT;
using Unity.XR.CoreUtils;
using UnityEngine;

namespace Gloxinia.Viker.Hero
{
    /// <summary>
    /// Hero の各機能へのアクセスポイントとしての役割を持つクラス
    /// </summary>
    internal class Hero : MonoBehaviour
    {
        [SerializeField]
        private XROrigin m_xrOrigin = null!;
        internal XROrigin XROrigin => m_xrOrigin;

        [SerializeField]
        private GameObject m_cameraAdjuster = null!;
        internal GameObject CameraAdjuster => m_cameraAdjuster;

        [SerializeField]
        private GVDirectInteractor m_leftHand = null!;
        internal GVDirectInteractor LeftHand => m_leftHand;

        [SerializeField]
        private GVDirectInteractor m_rightHand = null!;
        internal GVDirectInteractor RightHand => m_rightHand;
    }
}
