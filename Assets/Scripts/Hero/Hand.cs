#nullable enable

using UnityEngine;

namespace Gloxinia.Viker.Hero
{
    /// <summary>
    /// Hero の手の機能を司るクラス
    /// </summary>
    internal class Hand : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_手のモデル = null!;

        internal void 手のモデルを表示する()
        {
            m_手のモデル.SetActive(true);
        }

        internal void 手のモデルを非表示にする()
        {
            m_手のモデル.SetActive(false);
        }
    }
}
