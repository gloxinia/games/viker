#nullable enable

using UnityEngine;

namespace Gloxinia.Viker.Developments
{
    public class NotVR_バイク開発用 : MonoBehaviour
    {
        [SerializeField]
        private Rigidbody m_rigidbody = null!;

        [SerializeField]
        private WheelCollider m_前輪 = null!;

        [SerializeField]
        private WheelCollider m_後輪 = null!;

        private void Awake()
        {
            m_rigidbody.centerOfMass = new Vector3(0f, -0.5f, 0f);
            m_後輪.motorTorque = 200f;
            m_前輪.steerAngle = 15f;
        }
    }
}
