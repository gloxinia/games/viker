#nullable enable

using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Gloxinia.Viker.Developments
{
    public class シーンをリロードするスクリプト : MonoBehaviour
    {
        [SerializeField]
        private InputActionProperty m_ゲームをリセットする;

        private void Awake()
        {
            m_ゲームをリセットする.action.Enable();
            m_ゲームをリセットする.action.performed += ゲームをリセットする;
        }

        private void OnDestroy()
        {
            m_ゲームをリセットする.action.performed -= ゲームをリセットする;
            m_ゲームをリセットする.action.Disable();
        }

        private void ゲームをリセットする(InputAction.CallbackContext callbackContext)
        {
            SceneManager.LoadScene(0);
        }
    }
}
