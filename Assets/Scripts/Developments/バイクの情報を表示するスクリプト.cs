#nullable enable

using TMPro;
using UnityEngine;

namespace Gloxinia.Viker.Developments
{
    using Bike = Bike.Bike;

    public class バイクの情報を表示するスクリプト : MonoBehaviour
    {
        [SerializeField]
        private Bike m_bike = null!;

        [SerializeField]
        private TMP_Text m_速度 = null!;

        [SerializeField]
        private TMP_Text m_体の傾きの強さ = null!;

        [SerializeField]
        private TMP_Text m_アクセルの強さ = null!;

        [SerializeField]
        private TMP_Text m_ハンドブレーキの強さ = null!;

        [SerializeField]
        private TMP_Text m_クラッチの強さ = null!;

        [SerializeField]
        private TMP_Text m_motorTorque = null!;

        [SerializeField]
        private TMP_Text m_brakeTorque = null!;

        [SerializeField]
        private TMP_Text m_steerAngle = null!;

        private void Update()
        {
            m_速度.text = Mathf.FloorToInt(m_bike.キロメートル毎時).ToString("D3") + " km/h";
            m_体の傾きの強さ.text = "CE: " + m_bike.Turn.体の傾きの強さ.ToString("F2");

            // Grip
            m_アクセルの強さ.text = "AC: " + m_bike.GripRight.スロットルの強さ.ToString("F2");
            m_ハンドブレーキの強さ.text = "HB: " + m_bike.GripRight.レバーの強さ.ToString("F2");
            m_クラッチの強さ.text = "CL: " + m_bike.GripLeft.レバーの強さ.ToString("F2");

            // Wheel Collider
            m_motorTorque.text = "MT: " + m_bike.WheelBack.motorTorque.ToString("F2");
            m_brakeTorque.text = "BT: " + m_bike.WheelBack.brakeTorque.ToString("F2");
            m_steerAngle.text = "SA: " + m_bike.WheelFront.steerAngle.ToString("F2");
        }
    }
}
