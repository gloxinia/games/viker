#nullable enable

using Gloxinia.Viker.Hero;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Gloxinia.Viker.XRIT
{
    /// <summary>
    /// GLOXINIA VIKER 独自の実装が入った XRDirectInteractor
    /// </summary>
    internal class GVDirectInteractor : XRDirectInteractor
    {
        [Header("GV ならではのインスペクタ")]

        [SerializeField]
        private Hand m_heroHand = null!;
        internal Hand HeroHand => m_heroHand;
    }
}
